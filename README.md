# Student Presence

Android aplikacija napravljena za kolegij Osnove razvoja mobilnih aplikacija u kojoj se vježba RecyclerView. 
Po defaultu su dodana dva "studenta", a korisnik ih može naknadno dodavati neograničeno puta.
Pritiskom na studenta, iskoči kratka Toast poruka sa informacijama o studentu. Isto vrijedi i za brisanje korisnika.
Aplikaciju sam testirao na Xiaomi Redmi Note 8T (fizički uređaj) te na Google Pixel 4 (emulator, API 29).

## Screenshot:
<img src="/uploads/d3adcc1b44dfb65f3d504a685c12ee55/screenshot.jpg" width="400" height="800"/>

