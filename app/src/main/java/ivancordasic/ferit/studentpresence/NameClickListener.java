package ivancordasic.ferit.studentpresence;

public interface NameClickListener {
    void onNameClick(int position);
    void onRemoveClick(int position);
}
