package ivancordasic.ferit.studentpresence;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener {
    EditText etName;
    TextView tvName;
    Button btnDelete;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialieUI();
        setupRecycler();
        setupRecyclerData();
    }
    public void initialieUI(){
        this.etName = (EditText)findViewById(R.id.etName);
        this.btnDelete = (Button) findViewById(R.id.btnDelete);
        this.tvName = (TextView) findViewById(R.id.tvName);
    }

    private void setupRecycler(){
        RecyclerView recycler = findViewById(R.id.recyclerView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        this.adapter = new RecyclerAdapter(this);
        recycler.setAdapter(this.adapter);
    }
    private void setupRecyclerData(){
        List<String> data = new ArrayList<>();
        data.add("Jesus");
        data.add("Devil");
        adapter.addData(data);
    }

    public void addCell(View view) {
        if(this.etName.getText().toString().equals("")) {
            Toast.makeText(this,"Error! Name needed!", Toast.LENGTH_SHORT).show();
            return;
        }
        this.adapter.addCell(etName.getText().toString());
        this.etName.setText(null);
        this.etName.setHint("Enter name:");
    }

    @Override
    public void onNameClick(int position) {
        Toast.makeText(this, "Student at position: " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoveClick(int position) {
        this.adapter.deleteCell(position);
        Toast.makeText(this, "Student at position " + position + " removed!", Toast.LENGTH_SHORT).show();

    }
}