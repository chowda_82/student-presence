package ivancordasic.ferit.studentpresence;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder {
    private TextView tvName;
    public NameViewHolder(@NonNull View itemView, final NameClickListener listener) {
        super(itemView);
        this.tvName = itemView.findViewById(R.id.tvName);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onNameClick(getAdapterPosition());
            }
        });
        ImageView ivDelete = itemView.findViewById(R.id.ivDelete);
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemoveClick(getAdapterPosition());
            }
        });
    }
    public void setName(String name){
        tvName.setText(name);
    }
}
